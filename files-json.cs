﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace dupload_me
{
	[DataContract]
	class JsonRecords
	{
		[DataMember( Name = "file_version" )]
		public string file_version { get; set; }

		[DataMember( Name = "product_version" )]
		public string product_version { get; set; }

		[DataMember( Name = "files" )]
		public JsonRecordFile[] files { get; set; }
	}

	[DataContract]
	class JsonRecordFile
	{
		[DataMember( Name = "name" )]
		public string name { get; set; }

		[DataMember( Name = "url" )]
		public string url { get; set; }

		[DataMember( Name = "md5" )]
		public string md5 { get; set; }
	}
}
