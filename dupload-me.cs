﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Security.Cryptography;

namespace dupload_me
{
	public partial class dUpload_me : Form
	{
		public dUpload_me()
		{
			InitializeComponent();
		}

		private void dUpload_me_Load( object sender, EventArgs e )
		{
			textBox1.AppendText( "Checking for updates...\n" );
			getUpdateList();
		}

		private void getUpdateList()
		{
			var client = new System.Net.WebClient();
			client.DownloadStringCompleted += (sender, e) => 
			{
			   var stream = new MemoryStream( Encoding.UTF8.GetBytes( e.Result ) );

				DataContractJsonSerializer serailizer = new DataContractJsonSerializer( typeof( JsonRecords ) );
				JsonRecords records = ( JsonRecords )serailizer.ReadObject( stream );

				textBox1.AppendText( "File version: " + records.file_version + "\n" );
				textBox1.AppendText( "Product version: " + records.product_version + "\n" );
				textBox1.AppendText( "\n" );

				foreach( JsonRecordFile file in records.files ) {
					textBox1.AppendText( "File name: " + file.name + "\n" );

					var md5 = string.Empty;
					if ( File.Exists( file.name ) ) {
						StringBuilder sb = new StringBuilder();
						MD5 md5Hasher = MD5.Create();

						using ( FileStream fs = File.OpenRead( file.name ) )
						{
							foreach ( Byte b in md5Hasher.ComputeHash( fs ) )
								sb.Append( b.ToString( "x2" ).ToLower() );
						}
						md5 = sb.ToString();
					}

					if ( md5 != file.md5 ) {
						textBox1.AppendText( "Downloading: " + file.name + "...\n" );

						if ( File.Exists( file.name ) ) {
							File.Delete( file.name );
						}

						var directory = Path.GetDirectoryName( file.name ) + @"\";
						if ( !Directory.Exists( directory ) )
						{
							Directory.CreateDirectory( directory );
						}

						client.DownloadFile( file.url, file.name );
					}

					textBox1.AppendText( "\n" );
				}

				textBox1.AppendText( "Update completed!\n" );
			};

			client.DownloadStringAsync( new Uri( "http://dupload.vfc.cc/updates/files.json" ) );
		}
	}
}
